import React, { useEffect, useState } from 'react'
import axios from "axios"

const Photos = ()=> {
const [image, setImage] = useState("")

    useEffect(() => {
       axios.get("https://api.generated.photos/api/v1/faces?version=3&gender=female&age=young-adult&ethnicity=asian&hair_length=medium&order_by=random&emotion=joy",{
         headers:{
            Authorization: `API-Key w28bmagENyh8XisTUZ25KA`
         }, 
       })
       .then((res)=>{
        console.log(res.data.faces[0].urls[4][512]);
        setImage(res.data.faces[0].urls[4][512]);
       })
       .catch((err)=>{
        console.log(err.message);
       });
        
    }, [])
    return (
        <>
        {/* 
        https://generated.photos/
        https://www.youtube.com/watch?v=FRut1Nb-Gzs&ab_channel=Turbo360
        https://www.youtube.com/watch?v=L2jJIBNvMmI&list=PLO7dK6dlKmds2WhQdoZ2C0QRPsLdIq4Pu&ab_channel=Turbo360
         */}
          <img src={image} alt="photos" />
        </>
    )
}

export default Photos
